## SDN-based mobility

By
 
1. manually install flooding rules during the Mobile Node's movement, aka before the controller reacts to this movement;
2. install new rules based on new topology

this project is trying to provide an SDN based mobility protocol.

### Working pieces
The entrance of this project is `pyretic/examples/script_mobility.py`.
 
It glues many pieces together, including SDN controller, the topology and tcpdumper. 

Those modules are located in the same directory.

#### How to use
By looking at the commits history, locate the commit you are interested in, checkout to it. And then run

`sudo python pyretic/examples/script_mobility.py`

Output timestamp should be self-explanatory.

### Branches
There are three functional branches.

- update_cn: 
- do_nothing: 
- iperf: used to run iperf test

Apparently there are more branches though..






----------
## The following are the original README.md from pyretic project.
Pyretic
=======

The Pyretic platform - language & runtime system.
See http://frenetic-lang.org/pyretic/ for more info.

top-level structure:
- of_client:    Clients that run on a traditional OpenFlow controller
                effectively serving as a backend for Pyretic
- mininet:      Slightly modified version of mininet mn 
                and extra topologies used by Pyretic 
- mininet.sh:   A wrapper that starts up mininet for use w/ Pyretic
- pyretic:      Pyretic system proper 
- pyretic.py:   A wrapper that starts up Pyretic
                and optionally an OpenFlow client (see above)